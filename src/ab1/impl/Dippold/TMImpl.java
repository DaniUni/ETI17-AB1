package ab1.impl.Dippold;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ab1.TM;

public class TMImpl implements TM {
	private List<String> tapes = new ArrayList<>();
	private List<Integer> positions = new ArrayList<>();
	private Set<Character> symbols = new HashSet<>();
	private Set<TMTransition> transitions = new HashSet<>();
	private int state = 0;
	private enum Modes {running, halted, crashed}
	private Modes mode = Modes.running;

	@Override
	public void reset() {
		tapes = new ArrayList<>();
		positions = new ArrayList<>();
		symbols = new HashSet<>();

	}

	@Override
	public void setNumberOfTapes(int numTapes) throws IllegalArgumentException {
		if(numTapes < 1) throw new IllegalArgumentException();
		tapes = new ArrayList<>();
		for(int i=0; i<numTapes; i++){
			tapes.add(new String());
			positions.add(0);
		}
	}

	@Override
	public void setSymbols(Set<Character> symbols) throws IllegalArgumentException {
		if(!symbols.contains('#')) throw new IllegalArgumentException();
		this.symbols = symbols;
	}

	@Override
	public Set<Character> getSymbols() {
		return this.symbols;
	}

	@Override
	public void addTransition(int fromState, int tapeRead, char symbolRead, int toState, int tapeWrite,
			char symbolWrite, int tapeMove, Movement movement) throws IllegalArgumentException {

		if(fromState == 0 && movement == Movement.Stay) throw new IllegalArgumentException(); // wenn ausgehend vom Haltezustand ein Zeichen gelesen wird
		if(fromState < 0) throw new IllegalArgumentException(); // wenn eine Transition nicht eindeutig ist (fromState, symbolRead)
		if(!symbols.contains(symbolWrite)) throw new IllegalArgumentException(); // wenn ein Symbol nicht verarbeitet werden kann
		if(tapes.size() <= tapeMove || tapes.size() <= tapeRead || tapes.size() <= tapeWrite) throw new IllegalArgumentException(); // wenn das Band nicht existiert

		transitions.add(new TMTransition(fromState, tapeRead, symbolRead, toState, tapeWrite, symbolWrite, tapeMove, movement));
	}

	@Override
	public Set<Integer> getStates() {
		return getStates();
	}

	@Override
	public int getNumberOfTapes() {
		return tapes.size();
	}

	@Override
	public void setInitialState(int state) {
		this.state = state;
	}

	@Override
	public void setInitialTapeContent(int tape, char[] content) {
		for(char c : content){
			if(!symbols.contains(c)) throw new IllegalArgumentException();
		}
		String newTape = new String(content).endsWith("#") ? new String(content) : new String(content)+"#";
		tapes.set(tape, newTape);
		positions.set(tape, newTape.length()-1);
	}

	@Override
	public void doNextStep() throws IllegalStateException {
		for(TMTransition transition : transitions){
			if(transition.getFromState() == state && transition.getSymbolRead() == readSymbol(transition.getTapeRead())){
				int tapeIndex = transition.getTapeMove();
				switch (transition.getMovement()){
					case Left:
						positions.set(tapeIndex, positions.get(tapeIndex)-1);
						if(positions.get(tapeIndex) < 0){
							mode = Modes.crashed;
							throw new IllegalStateException();
						}
						while(tapes.get(tapeIndex).endsWith("##")) tapes.set(tapeIndex, tapes.get(tapeIndex).substring(0, tapes.get(tapeIndex).length()-1));
						break;

					case Right:
						positions.set(tapeIndex, positions.get(tapeIndex)+1);
						if(tapes.get(tapeIndex).length() <= positions.get(tapeIndex)) tapes.set(tapeIndex, tapes.get(tapeIndex)+"#");
						break;

					case Stay:
						writeSymbol(transition.getTapeWrite(), transition.getSymbolWrite());
						break;
				}
				state = transition.getToState();
				if(transition.getToState() == 0) mode = Modes.halted;
				break;
			}
		}
		
	}

	private char readSymbol(int tape){
		return tapes.get(tape).charAt(positions.get(tape));
	}

	private void writeSymbol(int tape, char symbol){
		char[] workingTape = tapes.get(tape).toCharArray();
		workingTape[positions.get(tape)] = symbol;

		tapes.set(tape, new String(workingTape));
	}

	@Override
	public boolean isHalt() {
		return mode == Modes.halted;
	}

	@Override
	public boolean isCrashed() {
		return mode == Modes.crashed;
	}

	@Override
	public List<TMConfig> getTMConfig() {
		if(isCrashed())	return null;
		List<TMConfig> tmConfigs = new ArrayList<>();
		for(int i=0; i<tapes.size(); i++){
			tmConfigs.add(getTMConfig(i));
		}
		return tmConfigs;
	}

	@Override
	public TMConfig getTMConfig(int tape) {
		char[] leftOfHead;
		if(positions.get(tape) == 0) leftOfHead = new char[0];
		else leftOfHead = tapes.get(tape).substring(0, positions.get(tape)-1).toCharArray();

		char belowHead;
		String sTape = tapes.get(tape);
		int pos = positions.get(tape);
		if(pos == 0) belowHead = sTape.charAt(0);
		else belowHead = sTape.charAt(pos);

		char[] rightOfHead;
		if(positions.get(tape) == tapes.get(tape).length()-1)
			rightOfHead = new char[0];
		else rightOfHead = tapes.get(tape).substring(positions.get(tape)-1).toCharArray();

		return new TMConfig(leftOfHead, belowHead, rightOfHead);
	}

	@Override
	public int getActState() {
		return state;
	}

	
}