package ab1.impl.Dippold;

import ab1.TM;

public class TMTransition {
    private int fromState;
    private int tapeRead;
    private char symbolRead;
    private int toState;
    private int tapeWrite;
    private char symbolWrite;
    private int tapeMove;
    private TM.Movement movement;

    public TMTransition(int fromState, int tapeRead, char symbolRead, int toState, int tapeWrite, char symbolWrite, int tapeMove, TM.Movement movement) {
        this.fromState = fromState;
        this.tapeRead = tapeRead;
        this.symbolRead = symbolRead;
        this.toState = toState;
        this.tapeWrite = tapeWrite;
        this.symbolWrite = symbolWrite;
        this.tapeMove = tapeMove;
        this.movement = movement;
    }

    public int getFromState() {
        return fromState;
    }

    public void setFromState(int fromState) {
        this.fromState = fromState;
    }

    public int getTapeRead() {
        return tapeRead;
    }

    public void setTapeRead(int tapeRead) {
        this.tapeRead = tapeRead;
    }

    public char getSymbolRead() {
        return symbolRead;
    }

    public void setSymbolRead(char symbolRead) {
        this.symbolRead = symbolRead;
    }

    public int getToState() {
        return toState;
    }

    public void setToState(int toState) {
        this.toState = toState;
    }

    public int getTapeWrite() {
        return tapeWrite;
    }

    public void setTapeWrite(int tapeWrite) {
        this.tapeWrite = tapeWrite;
    }

    public char getSymbolWrite() {
        return symbolWrite;
    }

    public void setSymbolWrite(char symbolWrite) {
        this.symbolWrite = symbolWrite;
    }

    public int getTapeMove() {
        return tapeMove;
    }

    public void setTapeMove(int tapeMove) {
        this.tapeMove = tapeMove;
    }

    public TM.Movement getMovement() {
        return movement;
    }

    public void setMovement(TM.Movement movement) {
        this.movement = movement;
    }
}
