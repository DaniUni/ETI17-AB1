package ab1.impl.Dippold;

import ab1.RegEx;

public class RegExImpl implements RegEx {

	@Override
	public String getRegexDomainName() {
		return "([a-z]+\\.[a-z]*\\.[a-z]{2,3})|([a-z]*\\.[a-z]{2,3})";
		// https://regexr.com/3h5or
	}

	@Override
	public String getRegexEmail() {
		return "[a-z]+([.-][a-z1-9]*)*@([a-z]*\\.[a-z]{2,3})";
		// https://regexr.com/3h5ou
	}

	@Override
	public String getRegexURL() {
		// TODO Auto-generated method stub
		return "(http|https|ftp):\\/\\/" + 									// Protocol
				"(([a-z]+\\.[a-z]*\\.[a-z]{2,3})|([a-z]*\\.[a-z]{2,3}))" + 	// Domain name
				"(([\\:][0-9]{2,})?" + 										// Ports
				"([/][a-z0-9.\\-/]*)?)"; 									// Rabbit hole
		// https://regexr.com/3h5p1
	}

	@Override
	public String multiMatch1() {
		return "[A-ZÄÖÜa-zäöü]*([^dr])oo[A-ZÄÖÜa-zäöü]*";
		// https://regexr.com/3h5q5
	}

	@Override
	public String multiMatch2() {
		return "[A-ZÄÖÜa-zäöü]+ick$";
		// https://regexr.com/3h5pm
	}

	@Override
	public String multiMatch3() {
		return "([a-z]{0,3}fu)$";
		// https://regexr.com/3h5q8
	}

	@Override
	public String multiMatch4() {
		return "(\\\\(w[+*])?($|\\?{3}))|" +							// \w+, \w*, \???
				"((a\\{[139](,8)?\\}\\|?)+)|" +							// a{1}|a{3}|a{9}, a{1,8}
				"(^-((\\+-)*\\+)?$)|" +									// -, -+-+-+-+
				"(\\(\\\\((w\\+\\?)|(ufacdf))\\)((\\\\7)|(\\*\\?)))|" +	// (\w+?)\7, (\ufacdf)*?
				"(\\[([A0akv]-[Z9foz])+\\][+*])|" +						// [A-Z0-9]+, [a-fk-ov-z]*
				"(\\(\\?:\\[aeiou\\]\\+\\)\\\\1\\+)|" +					// (?:[aeiou]+)\1+
				"(\\[a\\\\-z\\\\-9\\])|" +								// [a\-z\-9]
				"(\\(\\\\((001)|(2))\\)\\\\1)";							// (\001)\1, (\2)\1
		// This could definetly be more compact, but after three hours, I lost patience
		// https://regexr.com/3h6ut
	}
}