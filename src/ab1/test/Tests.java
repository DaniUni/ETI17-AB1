package ab1.test;

public class Tests {
    public static void main(String[] args){
        RegExTest regTests = new RegExTest();
        regTests.testRegexDomainName();
        regTests.testRegexEmail();
        regTests.testRegexURL();

        regTests.testMatch1();
        regTests.testMatch2();
        regTests.testMatch3();
        regTests.testMatch4();

        TMTest tmTests = new TMTest();
        //tmTests.testMachineOneTape();
        tmTests.testMachineTwoTapes();
    }
}
